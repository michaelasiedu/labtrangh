package com.labtrans.ejb.entities;

import com.labtrans.ejb.entities.TestParams;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-04-03T22:10:13")
@StaticMetamodel(TestParamDefaultValues.class)
public class TestParamDefaultValues_ { 

    public static volatile SingularAttribute<TestParamDefaultValues, String> deleted;
    public static volatile SingularAttribute<TestParamDefaultValues, Integer> testParamDefaultValuesId;
    public static volatile SingularAttribute<TestParamDefaultValues, TestParams> testParam;
    public static volatile SingularAttribute<TestParamDefaultValues, String> value;

}